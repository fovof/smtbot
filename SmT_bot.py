import telebot
import requests
import json
import os
import time

from PIL import Image, ImageDraw
import face_recognition

import psycopg2

#Место установки 
storage_path = os.getcwd()

#Добавление транзакта (идентификатор пользователя и путь к wav аудиофайлу) к базе данных
def add_to_db(uid, path):
	#Подключаемся к базе данных 
	conn = psycopg2.connect(dbname='users_wav', user='wavc', 
                        password='JamessON', host='localhost')
	cursor = conn.cursor()

	#Добавляем значения к таблице
	cursor.execute('INSERT INTO id_to_wac (uid, path_to_wav) VALUES (%s,%s)',(int(uid), path))
	
	#Для отладки
	#cursor.execute('SELECT * FROM id_to_wac')
	#print(cursor.fetchall())

	#Подтверждаем изменения и закрываем сеанс связи с бд
	conn.commit()
	cursor.close()
	conn.close()

#Конвертация файла в формат wav
def convert_to_wav(path):
	cmd = storage_path + r"\ffmpeg.exe -i " + path + " -ar 16000 " + path + ".wav"
	os.system(cmd)

#Сохранение контента сообщения (вложенного файла)
def save_content(message):

	#Находим идентификатор файла в зависимости от типа сообщения
	if message.content_type == 'document':
		file_id = message.document.file_id
	elif message.content_type == 'audio':
		file_id = message.audio.file_id
	elif message.content_type == 'photo':
		file_id = message.photo[0].file_id
	else:
		print("Not saveable")

	#Находим идентификатор пользователя
	user_id = str(message.from_user.id)

	#Отправляем GET запрос на получение ссылки на скачивание
	file_info_response = requests.get(url + 'getFile?file_id=' + file_id).json()	
	#print(message, file_info_response)
	
	#Получаем путь к файлу из ответа
	file_path = file_info_response['result']['file_path']

	#Формируем ссылку на скачивание и GET запросом скачиваем файл
	file_url = 'https://api.telegram.org/file/bot946376301:AAG0m8mFN5hhDkudQgf2Hl-yYqQ8IS1JMN8/' + file_path
	file = requests.get(file_url, allow_redirects=True)
	#print(url + file_path)
	
	#Пробуем создать папку для пользователя
	try:
		os.mkdir(user_id)
	except:
		pass

	#print(len(file.content), file.content, file)
	
	#Сохраняем файл по пути Путь_к_Установке/UID/file_N
	file_path_full = storage_path + '/' + user_id + '/' + file_path.split('/')[1]
	open(file_path_full,'wb').write(file.content)

	return file_path_full, user_id

url = "https://api.telegram.org/bot946376301:AAG0m8mFN5hhDkudQgf2Hl-yYqQ8IS1JMN8/"
bot = telebot.TeleBot("946376301:AAG0m8mFN5hhDkudQgf2Hl-yYqQ8IS1JMN8")

response = requests.get(url + 'KeyboardButton?text="/start"').json()

#Вывод справки
@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
	#Предполагается перенести в отдельный поток, чтобы не висело пока идет задержка
	user_id = message.from_user.id
	bot.send_message(user_id, "Hey, let me introduce myself!")
	file = open(storage_path + "/init.png",'rb').read()
	bot.send_photo(user_id, file)
	time.sleep(2)
	bot.send_message(user_id, "Yes, it's me.. As you can see, I have no face. Although I learned to recognize your human faces.")
	time.sleep(2)
	bot.send_message(user_id, "Just send me your photo, don't be so afraid...")
	time.sleep(2)
	bot.send_message(user_id, "You send me kinda this")
	file = open(storage_path + "/input.jpg",'rb').read()
	bot.send_photo(user_id, file)
	time.sleep(2)
	bot.send_message(user_id, "And you get respond with this")
	file = open(storage_path + "/output.jpg",'rb').read()
	bot.send_photo(user_id, file)
	time.sleep(2)
	bot.send_message(user_id, "Also, I collect your human music (for the times when you'll be done). So send me some of your favourites!")

#Реагирование на аудио файлы
@bot.message_handler(content_types=['audio'])
def collect_audio(message):
	
	#bot.send_message(message.chat.id, 'Поймал музыку!')

	#сохраняем контент сообщения
	file_path, user_id = save_content(message)

	#конвертируем полученный файл и удаляем старый
	convert_to_wav(file_path)
	os.remove(file_path)

	#добавляем запись в БД
	add_to_db(user_id, file_path + ".wav")

	#отправляем конвертированый файл обратно (Дебаг?)
	file = open(file_path + ".wav",'rb').read()
	bot.send_audio(chat_id=user_id, audio=file, title="wav16kHz.wav")


#Реагирование на фотографии
@bot.message_handler(content_types=['photo'])
def analyse_photo(message):
	
	#bot.send_message(message.chat.id, 'Поймал фотку!')
	#print(message)

	file_path, user_id = save_content(message)

	#Грузим изображение модулем face_recognition и им же ищем места с лицами
	image = face_recognition.load_image_file(file_path)
	face_locations = face_recognition.face_locations(image)

	if len(face_locations) >= 1:

		#Снова открывается изображение, но уже для рисования
		image = Image.open(file_path)
		draw = ImageDraw.Draw(image)

		#Во всех локациях с лицами рисуются прямоугольники
		for location in face_locations:
			draw.rectangle([location[1],location[0],location[3], location[2]], outline="red")

		del draw

		#Сохранение временного файла для отправки пользователю
		temp_path = file_path.split(".")[0] + "temp" + file_path.split(".")[1]
		image.save(temp_path, "PNG")#file_path.split(".")[1])
		print(face_locations)

		file = open(temp_path,'rb').read()
		bot.send_photo(chat_id=user_id, photo=file)
		os.remove(temp_path)
	else:
		bot.reply_to(message, "No faces found.")
		os.remove(file_path)

bot.polling()